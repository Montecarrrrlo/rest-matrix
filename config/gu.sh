#!/usr/bin/env bash
cd /home/ubuntu/rest-matrix/rest_matrix
exec gunicorn rest_matrix.wsgi:application \
  --bind 0.0.0.0:80
  --name "rest_matrix" \
  --workers 3 \
  --log-level=debug