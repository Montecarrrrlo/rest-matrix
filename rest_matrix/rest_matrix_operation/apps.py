from django.apps import AppConfig


class RestMatrixOperationConfig(AppConfig):
    name = 'rest_matrix_operation'
