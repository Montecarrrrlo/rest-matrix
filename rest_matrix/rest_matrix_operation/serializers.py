from .models import Matrix
from rest_framework import serializers



class UploadMatrixSerializer(serializers.ModelSerializer):

    class Meta:
        model = Matrix
        fields = '__all__'

class DisplayMatrixSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Matrix
        exclude = ('file', )