from django.db import models
import os, uuid
from datetime import date

# Create your models here.

SUPPORTED_FORMAT = (
    ('Numpy', 'Numpy'),
)

def matrix_upload_to(instance, filename):
    _, ext = os.path.splitext(filename)
    return "matrix/{0}-{1}{2}".format(
        date.today().strftime("%y%m%d"),
        uuid.uuid4(),
        ext
    )

from .validators import validate_file_extension

class Matrix(models.Model):
    file = models.FileField(upload_to=matrix_upload_to, validators=[validate_file_extension, ])
    format = models.CharField(max_length=128, choices=SUPPORTED_FORMAT)

from django.dispatch import receiver

@receiver(models.signals.post_delete, sender=Matrix)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)

