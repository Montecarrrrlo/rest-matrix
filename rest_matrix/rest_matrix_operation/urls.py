from django.urls import path, include

from rest_matrix_operation import views

matrix_list = views.MatrixViewset.as_view({
    'get':'list',
    'post':'create'
})

matrix_detail = views.MatrixViewset.as_view({
    'get':'retrieve',
    'delete':'destroy'
})


urlpatterns = [
    path('matrix', matrix_list, name="matrix-list"),
    path('matrix/<int:pk>', matrix_detail, name='matrix-detail'),
    path('matrix/<int:pk1>/multiply/<int:pk2>/', views.Multiply.as_view(), name='matrix-multiply'),
    path('matrix/<int:pk>/transpose', views.Transpose.as_view(), name='matrix-transpose')
]
