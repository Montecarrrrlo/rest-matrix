from rest_framework import viewsets
from .models import Matrix
from .serializers import UploadMatrixSerializer, DisplayMatrixSerializer
from django.http import FileResponse

class MatrixViewset(viewsets.ModelViewSet):

    queryset = Matrix.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return UploadMatrixSerializer
        return DisplayMatrixSerializer

    def retrieve(self, request, *args, **kwargs):
        if self.request.query_params.get('alt') == 'media':
            instance = self.get_object()
            response = FileResponse(instance.file.open())
            response['Content-Length'] = instance.file.size
            response['Content-Disposition'] = \
                'attachment; filename="{:s}"'.format(os.path.basename(instance.file.name))
            return response
        return super(MatrixViewset, self).retrieve(request, *args, **kwargs)

import os
import numpy as np
from rest_framework import generics
from django.core.files import File
from django.conf import settings
from tempfile import NamedTemporaryFile

def load_numpy(instance):
    fullpath = os.path.join(settings.MEDIA_ROOT, instance.file.name)
    return np.load(fullpath)

LOADER_METHODS = {
    'Numpy' : load_numpy
}


def save_numpy(mat):
    with File(NamedTemporaryFile(suffix='.npy')) as matf:
        np.save(matf, mat)
        res = Matrix.objects.create(file=matf, format='Numpy')
    return res

SAVER_METHODS = {
    'Numpy' : save_numpy,
}

class BinaryOperation(generics.RetrieveAPIView):
    serializer_class = DisplayMatrixSerializer

    def get_object(self):
        op1 = Matrix.objects.get(pk=self.kwargs['pk1'])
        op2 = Matrix.objects.get(pk=self.kwargs['pk2'])
        if op1.format != op2.format:
            raise ValueError('Invalid format for matrix.')
        load = LOADER_METHODS.get(op1.format, None)
        save = SAVER_METHODS.get(op1.format, None)
        if not load or not save:
            raise ValueError('No load or save format for matrix.')
        mat1 = load(op1)
        mat2 = load(op2)
        mat3 = self.operate(mat1, mat2)
        return save(mat3)

    def operate(self, op1, op2):
        raise NotImplemented('Base binary operations')

class Multiply(BinaryOperation):
    def operate(self, op1, op2):
        return np.matmul(op1, op2)

class UnaryOperation(generics.RetrieveAPIView):
    serializer_class = DisplayMatrixSerializer

    def get_object(self):
        op = Matrix.objects.get(pk=self.kwargs['pk'])
        load = LOADER_METHODS.get(op.format, None)
        save = SAVER_METHODS.get(op.format, None)
        if not load or not save:
            raise ValueError('No load or save format for matrix.')
        mat = load(op)
        mat2 = self.operate(mat)
        return save(mat2)

    def operate(self, mat):
        raise NotImplemented('Base unary operations')

class Transpose(UnaryOperation):

    def operate(self, mat):
        return mat.transpose()