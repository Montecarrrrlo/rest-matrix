from rest_framework.views import exception_handler
from rest_matrix_operation.views import BinaryOperation
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if isinstance(context['view'], BinaryOperation) and isinstance(exc, ValueError):
        return Response(
            data={"detail": str(exc)},
            status=HTTP_400_BAD_REQUEST,
        )

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code

    return response
