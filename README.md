# Rest-Matrix

Nest secure outsourcing cloud client implementation

`rest-matrix` : a restful matrix computation API for our secure outsourcing project

- Current API Functionalities
  - `GET /matrix/<pk>` returns a matrix indexed by the `pk`
  - `POST /matrix` uploads the matrix
  - `DELETE /matrix/<pk>` deletes the matrix indexed by `pk`
  - `GET /matrix/<pk1>/multiply/<pk2>` creates a new matrix that is the multiplication of matrices indexed by `pk1` and `pk2`
  - `GET /matrix/<pk1>` creates a new matrix that is the transpose of matrices indexed by `pk`
- Security issues
  - Currently HTTP protocol does not provide encryption.
  - Need CA signed HTTPS certificate to provide full protection
    - With HTTPS, passive and retrospective attack could be avoided
    - If not signed, could have "man-in-the-middle" attacked.
    - Requirements
      1. Have a public key (and associated private key)
      2. Have the key signed by the trusted CA
      3. Have a proper domain with name services configured